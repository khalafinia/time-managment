<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200205230712 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tasks (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_entry (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, tasks_id INT DEFAULT NULL, start DATE DEFAULT NULL, end DATE DEFAULT NULL, INDEX IDX_6E537C0CA76ED395 (user_id), INDEX IDX_6E537C0CE3272D31 (tasks_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_entry_time_categories (time_entry_id INT NOT NULL, time_categories_id INT NOT NULL, INDEX IDX_844615411EB30A8E (time_entry_id), INDEX IDX_844615412AC34638 (time_categories_id), PRIMARY KEY(time_entry_id, time_categories_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE time_entry ADD CONSTRAINT FK_6E537C0CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE time_entry ADD CONSTRAINT FK_6E537C0CE3272D31 FOREIGN KEY (tasks_id) REFERENCES tasks (id)');
        $this->addSql('ALTER TABLE time_entry_time_categories ADD CONSTRAINT FK_844615411EB30A8E FOREIGN KEY (time_entry_id) REFERENCES time_entry (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_entry_time_categories ADD CONSTRAINT FK_844615412AC34638 FOREIGN KEY (time_categories_id) REFERENCES time_categories (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE time_entry DROP FOREIGN KEY FK_6E537C0CE3272D31');
        $this->addSql('ALTER TABLE time_entry_time_categories DROP FOREIGN KEY FK_844615411EB30A8E');
        $this->addSql('DROP TABLE tasks');
        $this->addSql('DROP TABLE time_entry');
        $this->addSql('DROP TABLE time_entry_time_categories');
    }
}
