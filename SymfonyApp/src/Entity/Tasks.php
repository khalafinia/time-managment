<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TasksRepository")
 */
class Tasks
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeEntry", mappedBy="tasks")
     */
    private $timeEntry;

    public function __construct()
    {
        $this->timeEntry = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|TimeEntry[]
     */
    public function getTimeEntry(): Collection
    {
        return $this->timeEntry;
    }

    public function addTimeEntry(TimeEntry $timeEntry): self
    {
        if (!$this->timeEntry->contains($timeEntry)) {
            $this->timeEntry[] = $timeEntry;
            $timeEntry->setTasks($this);
        }

        return $this;
    }

    public function removeTimeEntry(TimeEntry $timeEntry): self
    {
        if ($this->timeEntry->contains($timeEntry)) {
            $this->timeEntry->removeElement($timeEntry);
            // set the owning side to null (unless already changed)
            if ($timeEntry->getTasks() === $this) {
                $timeEntry->setTasks(null);
            }
        }

        return $this;
    }
}
