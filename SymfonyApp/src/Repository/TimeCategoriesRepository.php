<?php

namespace App\Repository;

use App\Entity\TimeCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TimeCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeCategories[]    findAll()
 * @method TimeCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeCategories::class);
    }

    // /**
    //  * @return TimeCategories[] Returns an array of TimeCategories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TimeCategories
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
