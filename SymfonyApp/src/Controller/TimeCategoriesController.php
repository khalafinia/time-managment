<?php

namespace App\Controller;

use App\Entity\TimeCategories;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TimeCategoriesController extends AbstractController
{
    /**
     * @Route("/time/categories",methods={"GET"}, name="time_categories")
     */
    public function index()
    {
        $timeCatefories = $this->getDoctrine()
            ->getRepository(TimeCategories::class)
            ->findAll();

        return $this->json([
            'categories' => $timeCatefories,
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TimeCategoriesController.php',
        ]);
    }

    /**
     * @Route("/time/categories/{id}", methods={"GET"}, name="time_categories_get_one")
     */
    public function getOne(int $id, SerializerInterface $serializer)
    {
        /**  @var TimeCategories $timeCategory*/
        $timeCategory = $this->getDoctrine()
            ->getRepository(TimeCategories::class)
            ->find($id);;
//        $timeCategory->setCategory('test');

//        dd();exit();
        return $this->json([
            'category' => $serializer->serialize($timeCategory,'json'),
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TimeCategoriesController.php',
        ]);
    }

    /**
     * @Route("/time/categories",methods={"POST"}, name="time_categories_save_one")
     */
    public function saveOne(Request $requst)
    {
//        dd($requst->get('name'));exit;
        $timeCategory = new TimeCategories();
        $timeCategory->setCategory($requst->get('name'));
//        $timeCategory->setCreatedAt(new \DateTime());
//        $timeCategory->setUpdatedAt(new \DateTime());
        $timeCategory->setUserId(1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($timeCategory);
        $entityManager->flush();

        return $this->json([
            'category' => $timeCategory,
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TimeCategoriesController.php',
        ]);
    }

    /**
     * @Route("/time/categories/{id}",methods={"POST"}, name="time_categories_update_one")
     */
    public function updateOne(Request $requst,$id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $timeCategory = $this->getDoctrine()->getRepository(TimeCategories::class)->find($id);
        try {
            $category = $requst->get('name');
            $timeCategory->setCategory($category);

            $entityManager->persist($timeCategory);
            $entityManager->flush();

        } catch (\Error $e){
            return $this->json([$e->getMessage()]);
        }catch (\Exception $e){
            return $this->json([$e->getMessage()]);
        }

        return $this->json([
            'category' => $timeCategory,
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TimeCategoriesController.php',
        ]);
    }
}
