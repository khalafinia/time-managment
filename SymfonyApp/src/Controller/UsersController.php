<?php

namespace App\Controller;

use App\Entity\TimeCategories;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UsersController extends AbstractController
{
    /**
     * @Route("/users", name="users",methods={"GET"})
     */
    public function index()
    {
        $users = $this->getDoctrine()
            ->getRepository(Users::class)
            ->findAll();
        return $this->json(['users'=>$users
        ]);
    }

    /**
     * @Route("/users/{id}", methods={"GET"}, name="users_get_one")
     */
    public function getOne(int $id)
    {
        /**  @var Users $user*/
        $user = $this->getDoctrine()
            ->getRepository(TimeCategories::class)
            ->find($id);;
        return $this->json([
            'user' => $user,
        ]);
    }

    /**
     * @Route("/users", name="users_add",methods={"POST"})
     */
    public function addUser(Request $requst)
    {
        //        dd($requst->get('name'));exit;
        $users = new Users();
        $users->setUsername($requst->get('username'));
        $users->setPassword($requst->get('password'));
        $users->setEmail($requst->get('email'));
        $users->setApiToken('testtest');
        $users->setRoles($requst->get('roles'));


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($users);
        $entityManager->flush();

        return $this->json([
            'category' => $users,
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TimeCategoriesController.php',
        ]);
    }
}
