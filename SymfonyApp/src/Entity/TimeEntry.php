<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimeEntryRepository")
 */
class TimeEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $end;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="timeEntries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TimeCategories", inversedBy="timeEntries")
     */
    private $timeCategory;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tasks", inversedBy="timeEntry")
     */
    private $tasks;

    public function __construct()
    {
        $this->timeCategory = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(?\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }


    public function getUser(): ?int
    {
        return $this->user;
    }


    public function setUser(?Users $test): self
    {
        $this->test = $test;

        return $this;
    }

    /**
     * @return Collection|TimeCategories[]
     */
    public function getTimeCategory(): Collection
    {
        return $this->timeCategory;
    }

    public function addTimeCategory(TimeCategories $timeCategory): self
    {
        if (!$this->timeCategory->contains($timeCategory)) {
            $this->timeCategory[] = $timeCategory;
        }

        return $this;
    }

    public function removeTimeCategory(TimeCategories $timeCategory): self
    {
        if ($this->timeCategory->contains($timeCategory)) {
            $this->timeCategory->removeElement($timeCategory);
        }

        return $this;
    }

    public function getTasks(): ?Tasks
    {
        return $this->tasks;
    }

    public function setTasks(?Tasks $tasks): self
    {
        $this->tasks = $tasks;

        return $this;
    }
}
